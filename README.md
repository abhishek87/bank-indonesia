# SETUP INSTRUCTIONS
- Docker Version = 19.03.1
- Docker-Composer Version = 1.22.0
- python 3.6
- pip3
# Clone the repository
> - git clone https://gitlab.com/third-eye-dev-group/bank-indonesia.git 
> - cd bank-indonesia/

# Generate Zookeeper and multi-broker Kafka

> **For Linux Users:**
- docker-compose up -d
> **For Windows User**
- docker-compose up -f docker-compose-windows.yml

**WE RECOMMEND TO RUN THE DOCKER-COMPOSE IN A LINUX MACHINE**
## Check if containers are up
- docker ps -a
- docker logs kafka-1
- docker logs kafka-2
- docker logs zookeeper
## Producer-Consumer Setup
> Run a simple python application which will act as a Kafka producer and sends ‘transaction’ events.The application will send at least 1000 transactions across 20 customers across a single topic. A second application that acts as a Kafka consumer to subscribe to the topic and upon receipt of each message logs a tuple of (account_number, sum(amount)), i.e. the total value of the transactions made by the customer while the application has been running. A protobuf to serialize the messages.

- **python3 prouducer.py ##to generate the producer**
- **python3 consumer.py  ##to generate the consumer** 
 
